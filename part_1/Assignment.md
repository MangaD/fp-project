# Foundations of Programming

## Project - Part 1

25 October 2011

A prime number is a natural number that has only two divisors. For example, 5 is prime, as it is only divisible by 1 and by 5; on the other hand, 6 is not prime as 6 is divisible by 1, by 2, by 3 and by 6. Number 1 is not prime, as it has only one divisor, itself. Prime numbers have an essential role in number theory, expressed by the **fundamental theorem of arithmetic**: any positive integer *n > 1* may be written in a unique form, except for a reordering of the factors, as the product of prime powers, its prime factors.

A simple method, but not very efficient, to verify if a number *n* is prime, known as **trial division method**, consists in testing if *n* is a multiple of any integer between 2 and **&#8730;n**. There are much more efficient algorithms to test if a number is prime, but they will not be considered in this project. The largest prime number known in 2011 has about 13 million digits.

Many questions about prime numbers are still open problems. For example, **Goldbach's conjecture**, that affirms that any even integer greater than 2 can be expressed as the sum of two primes, and the **twin prime conjecture**, that affirms that there exist infinite twin primes (pairs of primes whose difference is 2). Despite being formulated more than a century ago, are still unsolved.

Prime numbers are used in many computer applications, for example, **public-key cryptography** that resorts to the difficulty in calculating prime factors of very large numbers.

## 1. Primality Test

Write a procedure in [Scheme](https://en.wikipedia.org/wiki/Scheme_(programming_language)), called `prime?`, that receives a positive integer as argument (verifying its input) and resorts to the trial division method in order to determine if its argument is a prime.

For example:

```scheme
> (prime? 5)
#t
> (prime? 6)
#f
```


## 2. Enumerating primes

In programming, an *iterator* is a program that traverses a set of elements. We can, for example, think about iterators that traverse positive integers, even numbers or prime numbers. A way of creating iterators is to resort to procedures known as *generators* that generate the successive elements of the set. For example, the following procedures generate integers and even numbers respectively:

```scheme
(define (nth-integer n)
  n)

(define (nth-even n)
  (* 2 n))
```

These procedures, from an arbitrary number *n*, generate, respectively, the nth positive integer (equal to *n*) and the nth even number.

It is evident that the concepts of iterator and generator are much more complex than presented here, but our definitions are meant to contextualise what is intended with this part of the project.

Write a procedure in Scheme, called `nth-prime`, that receives a positive integer *n* (verifying its input) and returns the nth prime number. Use the `prime?` procedure.

For example:

```scheme
> (nth-prime 5)
11
> (nth-prime 45)
197
```

