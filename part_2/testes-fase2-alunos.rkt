#lang scheme
(require (lib "misc.ss" "swindle")
         (lib "list.ss")
         (lib "lset.ss" "srfi" "1"))

;;=======================================
;; 1) Colocar o ficheiro na mesma directoria que o ficheiro com o código do projecto;
;; 2) Definir número do grupo a seguir;
;; 3) Executar a acção "Correr".
;;=======================================
(define *numero-do-grupo* "11")

;; TAI teste
(define (cria-teste nome forma erro?)
  (cons erro? (cons nome forma)))

;; Nome do teste
(define (nome-teste teste)
  (cadr teste))

;; Código a ser avaliado
(define (forma-teste teste)
  (cddr teste))

;; Indica se a avaliação do código deve gerar um erro de execução
(define (erro?-teste teste)
  (car teste))


;; Definição dos testes
(define (cria-testes)
  (list 
   (cria-teste "teste insere-conj 01"
               '(equal?
                 (insere-conj 1 5 )
                 'erro)
               #t)
   (cria-teste "teste faz-conj 01"
               '(conjuntos=?
                 (faz-conj 1 2 )
                 (insere-conj 2 (insere-conj 1 (novo-conj))))
               #f)
   (cria-teste "teste retira-conj 01"
               '(conjuntos=?
                 (retira-conj 1 (faz-conj 1 2 3) )
                 (faz-conj 2 3))
               #f)
   (cria-teste "teste retira-conj 02"
               '(conjuntos=?
                 (retira-conj 1 (faz-conj 2 3) )
                 (faz-conj 2 3))
               #f)
   (cria-teste "teste conjunto? 01"
               '(equal?
                 (conjunto? (novo-conj) )
                 #t)
               #f)
   (cria-teste "teste conjunto? 02"
               '(equal?
                 (conjunto? (faz-conj 1 2 3) )
                 #t)
               #f)
   (cria-teste "teste conjunto? 03"
               '(equal?
                 (conjunto? (insere-conj 1 (faz-conj 1 2 3)) )
                 #t)
               #f)
   (cria-teste "teste conjunto? 04"
               '(equal?
                 (conjunto? 1 )
                 #f)
               #f)
   (cria-teste "teste conj-vazio? 01"
               '(equal?
                 (conj-vazio? (novo-conj) )
                 #t)
               #f)
   (cria-teste "teste conj-vazio? 02"
               '(equal?
                 (conj-vazio? (faz-conj 1 2 3) )
                 #f)
               #f)
   (cria-teste "teste conj-vazio? 03"
               '(equal?
                 (conj-vazio? (insere-conj 1 (faz-conj 1 2 3)) )
                 #f)
               #f)
   (cria-teste "teste conjuntos=? 01"
               '(equal?
                 (conjuntos=? (faz-conj 1 2 3) (faz-conj 3 2 1) )
                 #t)
               #f)
   (cria-teste "teste conjuntos=? 02"
               '(equal?
                 (conjuntos=? (faz-conj 1 2 3) (faz-conj 3 2 1 3 2) )
                 #t)
               #f)
   (cria-teste "teste conjuntos=? 03"
               '(equal?
                 (conjuntos=? (faz-conj 1 2 3) (insere-conj 1 (faz-conj 1 2 3)) )
                 #t)
               #f)
   (cria-teste "teste conjuntos=? 04"
               '(equal?
                 (conjuntos=? (faz-conj 1 2 3) (insere-conj 6 (faz-conj 1 2 3)) )
                 #f)
               #f)
   (cria-teste "teste pertence? 01"
               '(equal?
                 (pertence? 3 (novo-conj) )
                 #f)
               #f)
   (cria-teste "teste pertence? 02"
               '(equal?
                 (pertence? 5 (faz-conj 1 2 3) )
                 #f)
               #f)
   (cria-teste "teste pertence? 03"
               '(equal?
                 (pertence? 4 (insere-conj 1 (faz-conj 1 2 3)) )
                 #f)
               #f)
   (cria-teste "teste pertence? 04"
               '(equal?
                 (pertence? 2 (faz-conj 1 2 3) )
                 #t)
               #f)
   (cria-teste "teste pertence? 05"
               '(equal?
                 (pertence? 2 (insere-conj 1 (faz-conj 1 2 3)) )
                 #t)
               #f)
   (cria-teste "teste todos-elementos-conjunto-satisfazem? 01"
               '(equal?
                 (todos-elementos-conjunto-satisfazem? (faz-conj 1 5 3) odd? )
                 #t)
               #f)
   (cria-teste "teste todos-elementos-conjunto-satisfazem? 02"
               '(equal?
                 (todos-elementos-conjunto-satisfazem? (faz-conj 1 2 5 3) odd? )
                 #f)
               #f)
   (cria-teste "teste conj->lista 01"
               '(equal?
                 (conj->lista (novo-conj) )
                 '())
               #f)
   (cria-teste "teste conj->lista 02"
               '(let ((lst1 (conj->lista (faz-conj 1 2 3)) )
                      (lst2 (list 1 2 3)))
                  (and (lset= equal? lst1 lst2)
                       (= (length lst1) (length lst2))))
               #f)
   (cria-teste "teste conj->lista 03"
               '(let ((lst1 (conj->lista (insere-conj 1 (faz-conj 1 2 3)) ))
                      (lst2 (list 1 2 3)))
                  (and (lset= equal? lst1 lst2)
                       (= (length lst1) (length lst2))))
               #f)
   (cria-teste "teste interseccao 01"
               '(conjuntos=?
                 (interseccao (faz-conj 1 2 3) (faz-conj 3 2 1) )
                 (faz-conj 1 2 3))
               #f)
   (cria-teste "teste interseccao 02"
               '(conjuntos=?
                 (interseccao (faz-conj 1 2 3) (faz-conj 3 1) )
                 (faz-conj 1 3))
               #f)
   (cria-teste "teste interseccao 03"
               '(conjuntos=?
                 (interseccao (faz-conj 1 2 3) (faz-conj 3 1 4) )
                 (faz-conj 1 3))
               #f)
   (cria-teste "teste faz-ramo 01"
               '(equal?
                 (faz-ramo 'A 'B 7.9 )
                 'erro)
               #t)
   (cria-teste "teste extremos-ramo 01"
               '(conjuntos=?
                 (extremos-ramo (faz-ramo 'A 'B 7) )
                 (faz-conj 'A 'B))
               #f)
   (cria-teste "teste rotulo-ramo 01"
               '(equal?
                 (rotulo-ramo (faz-ramo 'A 'B 7) )
                 7)
               #f)
   (cria-teste "teste outro-no-ramo 01"
               '(equal?
                 (outro-no-ramo 'A (faz-ramo 'A 'B 7) )
                 'B)
               #f)
   (cria-teste "teste ramo? 01"
               '(equal?
                 (ramo? (faz-ramo 'A 'B 7) )
                 #t)
               #f)
   (cria-teste "teste ramo? 02"
               '(equal?
                 (ramo? 3 )
                 #f)
               #f)
   (cria-teste "teste ramos=? 01"
               '(equal?
                 (ramos=? (faz-ramo 'A 'B 7) (faz-ramo 'B 'A 7) )
                 #t)
               #f)
   (cria-teste "teste ramos=? 02"
               '(equal?
                 (ramos=? (faz-ramo 'A 'B 7) (faz-ramo 'A 'B 5) )
                 #f)
               #f)
   (cria-teste "teste extremo-ramo? 01"
               '(equal?
                 (extremo-ramo? 'A (faz-ramo 'A 'B 7) )
                 #t)
               #f)
   (cria-teste "teste extremo-ramo? 02"
               '(equal?
                 (extremo-ramo? 'B (faz-ramo 'A 'B 7) )
                 #t)
               #f)
   (cria-teste "teste extremo-ramo? 03"
               '(equal?
                 (extremo-ramo? 'C (faz-ramo 'A 'B 7) )
                 #f)
               #f)
   (cria-teste "teste faz-grafo 01"
               '(equal?
                 (faz-grafo (faz-conj 'A 'B 'C) (faz-conj 4 (faz-ramo 'A 'C 7)) )
                 'erro)
               #t)
   (cria-teste "teste nos 01"
               '(conjuntos=?
                 (nos (faz-grafo (novo-conj) (novo-conj)) )
                 (faz-conj))
               #f)
   (cria-teste "teste nos 02"
               '(conjuntos=?
                 (nos (faz-grafo (faz-conj 'A 'B) (novo-conj)) )
                 (faz-conj 'A 'B))
               #f)
   (cria-teste "teste ramos 01"
               '(conjuntos=?
                 (ramos (faz-grafo (novo-conj) (novo-conj)) )
                 (faz-conj))
               #f)
   (cria-teste "teste ramos 02"
               '(conjuntos=?
                 (ramos (faz-grafo (novo-conj) (faz-conj (faz-ramo 'A 'B 7) (faz-ramo 'A 'C 7))) )
                 (faz-conj (faz-ramo 'A 'B 7) (faz-ramo 'A 'C 7)))
               #f)
   (cria-teste "teste vizinhos 01"
               '(conjuntos=?
                 (vizinhos 'A (faz-grafo (faz-conj 'A 'B 'C 'D) (faz-conj (faz-ramo 'A 'B 7) (faz-ramo 'A 'C 5))) )
                 (faz-conj 'B 'C))
               #f)
   (cria-teste "teste vizinhos 02"
               '(conjuntos=?
                 (vizinhos 'D (faz-grafo (faz-conj 'A 'B 'C 'D) (faz-conj (faz-ramo 'A 'B 7) (faz-ramo 'A 'C 5))) )
                 (novo-conj))
               #f)
   (cria-teste "teste distancia-entre 01"
               '(equal?
                 (distancia-entre 'A 'B (faz-grafo (faz-conj 'A 'B 'C 'D) (faz-conj (faz-ramo 'A 'B 7) (faz-ramo 'A 'C 5))) )
                 7)
               #f)
   (cria-teste "teste grafo? 01"
               '(equal?
                 (grafo? (faz-grafo (novo-conj) (novo-conj)) )
                 #t)
               #f)
   (cria-teste "teste grafo? 02"
               '(equal?
                 (grafo? (faz-grafo (faz-conj 'A 'B 'C 'D) (faz-conj (faz-ramo 'A 'B 7) (faz-ramo 'A 'C 5))) )
                 #t)
               #f)
   (cria-teste "teste grafo? 03"
               '(equal?
                 (grafo? 4 )
                 #f)
               #f)
   (cria-teste "teste grafos=? 01"
               '(equal?
                 (grafos=? (faz-grafo (faz-conj 'A 'B 'C 'D) (faz-conj (faz-ramo 'A 'B 7) (faz-ramo 'A 'C 5))) (faz-grafo (faz-conj 'A 'B 'C 'D) (faz-conj (faz-ramo 'A 'B 7) (faz-ramo 'A 'C 5))) )
                 #t)
               #f)
   (cria-teste "teste grafos=? 02"
               '(equal?
                 (grafos=? (faz-grafo (faz-conj 'A 'B 'C 'D) (faz-conj (faz-ramo 'A 'B 7) (faz-ramo 'A 'C 5))) (faz-grafo (faz-conj 'A 'B 'C) (faz-conj (faz-ramo 'A 'B 7) (faz-ramo 'A 'C 5))) )
                 #f)
               #f)
   ))

(define (testa testes)
  (dolist (teste testes)
          (let ((res (no-errors* (eval (forma-teste teste)))))
            (newline)
            (display (nome-teste teste))
            (cond ((struct? res) (if (erro?-teste teste)
                                     (display " - Passou.")
                                     (display " - FALHOU.")))
                  ((erro?-teste teste) (display " - FALHOU."))
                  (res (display " - Passou."))
                  (else (display " - FALHOU."))))))



(parameterize ([current-namespace (make-base-empty-namespace)])
  (namespace-require 'scheme)
  (namespace-require (string-append "fp1112-parte2-grupo" *numero-do-grupo* ".rkt"))
  (eval '(require (lib "misc.ss" "swindle")
                  (lib "list.ss")
                  (lib "lset.ss" "srfi" "1")
                  (lib "trace.ss")))
  (testa (cria-testes)))

