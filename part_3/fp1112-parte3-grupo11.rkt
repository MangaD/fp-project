#lang scheme
(provide (all-defined-out))
(require "solucao-parte2.rkt")

;Grupo nº11
;Daniel Freire Lascas nº73824
;David Manuel Sales Gonçalves nº73891
;Ricardo Manuel Mota de Moura nº74005


(require (lib "misc.ss" "swindle"))                  ;Para a utilização de ciclos


;;; TIPO INFO-NÓ ;;;

;;Construtor

(define (novo-info-no no)                            ;Este procedimento não verifica a validade do argumento,  pois um nó pode ser de qualquer tipo.
  (vector no +inf.0 null))                           ;devolve um vector cujo primeiro elemento é o nó 'no',  cuja distância é infinito,  e cujo  nó
                                                     ;anterior é indefinido (null).

;;Selectores

(define (no-info-no info-no)                         ;Devolve o nó, primeiro elemento do vector, do tipo info-nó facultado.
  (vector-ref info-no 0))

(define (dist-info-no info-no)                       ;Devolve a distância, segundo elemento do vector, do tipo info-nó facultado.
  (vector-ref info-no 1))

(define (ant-info-no info-no)                        ;Devolve o nó anterior, terceiro elemento do vector, do tipo info-nó facultado.
  (vector-ref info-no 2))

;;Modificadores

(define (modifica-dist! info-no int)                 ;Altera  a distância de 'info-no' para 'int' e devolve 'info-no'  com  a  distância  alterada.
  (vector-set! info-no 1 int)                        ;É utilizado o procedimento  'vector-set',  cujo  1º argumento  é  o  info-nó  cuja  distância
  info-no)                                           ;se pretende alterar,  o  2º argumento  é  a posição da distância no vector e o  3º  argumento
                                                     ;corresponde ao valor da nova distância a atribur ao info-nó.

(define (modifica-ant! info-no no)                   ;Altera o nó anterior de 'info-no' para 'no' e devolve 'info-no' com  o  no anterior alterado.
  (vector-set! info-no 2 no)                         ;É utilizado o procedimento 'vector-set',  cujo  1º argumento é  o info-nó  cujo  nó  anterior
  info-no)                                           ;se pretende alterar,  o  2º argumento é a posição do nó anterior no vector e o  3º  argumento
                                                     ;corresponde ao valor do novo nó anterior a atribur ao info-nó.


;;; TIPO INFO-NÓS ;;;

;;Construtor

(define (cria-info-inicial conj)                     ;Devolve a colecção de elementos do tipo info-nó obtidos por aplicação  de  'novo-info-no'  a
  (if (not (conjunto? conj))                         ;cada nó do conjunto 'conj'. É verificada a validade do argumento.
      (error "cria-info-inicial: O argumento tem que ser do tipo conjunto.")
      (let ((lista conj)                             ;É  criada uma nova variável 'lista' à qual é atribuída a estrutura e conteúdo  de  'conj'  e,
            (cont 0))                                ;também é criada uma variável 'cont' à qual é atribuída o valor zero.
        (do ((contador 0 (add1 contador)))           ;Neste ciclo 'do',  cria-se uma nova variável 'contador' que inicia com o valor zero, e evoluí
          ((lista-vazia? lista) (set! cont contador));somando 1 ao seu valor por cada iteração.  Na condição de paragem, se 'lista' estiver  vazia,
                                                     ;a variável 'cont' fica com o valor de 'contador'.  A variável 'cont' existe porque a variável
          (set! lista (resto lista)))                ;'contador' é local e, por isso, não pode ser utilizada fora do ciclo 'do'.  Por cada iteração
                                                     ;realizada, 'lista' perde o 1º elemento até que fique vazia.  Este ciclo serve para contar  os
                                                     ;elementos de 'conj'.
        (let ((info-inicial (make-vector cont))      ;É  criada uma nova variável 'info-inicial' ao qual é  atribuída  um  vector  com  comprimento
              (i 0))                                 ;igual a 'cont'. É também criada uma nova variável 'i' ao qual é atribuída o valor zero.
          (dolist (el conj)                          ;Neste ciclo 'dolist' são atribuídos os elementos de 'conj', um por cada iteração,  à variável
                  (vector-set! info-inicial          ;'el'.  Por cada iteração é actualizado o conteúdo de 'info-inicial' que na posição 'i' assume 
                               i                     ;um novo info-nó correspondente ao elemento a ser avaliado na respectiva iteração.  A variável
                               (novo-info-no el))    ;'i' tem como valor inícial zero ao qual será incrementado 1 por cada iteração até  que  todos
                  (set! i (add1 i)))                 ;os elementos de 'conj' tenham sido processados.   Este  ciclo  'dolist'  serve  para criar  a
          info-inicial))))                           ;a colecção de elementos do tipo info-nó correspondentes aos nós do conjunto 'conj'.



;;Selectores

(define (obtem-info-no info-nos no)                  ;Devolve o elemento de 'info-nos' cujo nó é 'no'.
  (let ((num-elems (vector-length info-nos)))        ;É criada uma nova variável 'num-elems' ao qual é atribuída o valor do comprimento  do  vector
                                                     ;'info-nos'.
    (do ((i 0 (add1 i)))                             ;Optámos pela utilização dum ciclo 'do' em vez dum ciclo  'dotimes'  devido  à  eficiência  do
                                                     ;do processo. Caso fosse utilizado um 'dotimes', os elementos do vector seriam processados até
                                                     ;ao fim. A variável 'i' inicia com o valor zero e adiciona 1 por cada iteração.
      ((or (= i num-elems)                           ;A ordem das condições no 'or' da condição de paragem interessa porque se o 'i'  for  igual  a
           (equal? (vector-ref (vector-ref info-nos i) 0);'num-elems' o procedimento vector-ref, caso surgisse antes na ordem, daria erro, pois tal
                   no))                              ;posição não existe no vector. A iteração para assim que 'i' for igual a 'num-elems' ou  o  nó
                                                     ;presente no info-no da posição 'i' do 'info-nos' for igual a 'no'.
       (if (= i num-elems)                           ;Caso 'i' seja igual a 'num-elems'  significa que 'no' não  se  encontra  presente  em  nenhum
           (error "obtem-info-no: o nó não se encontra presente em nenhum elemento do info-nós.")  ;info-nó  de 'info-nos' e, como tal, é devolvido
           (vector-ref info-nos i))))))              ;erro. Caso contrário, é devolvido o info-nó de 'info-nos' cujo nó é 'no'.


(define (no<dist info-nos conj)                      ;Devolve o nó do conjunto 'conj' com a menor distância associada em 'info-nos'.
  (let ((dist1 +inf.0)                               ;São criadas quatro variáveis, às quais são atribuídas valores iniciais,  que serão usadas  no
        (no-aux null)                                ;ciclo 'while'.
        (dist2 +inf.0)
        (no null))
    (while (not (lista-vazia? conj))                 ;Enquanto 'conj' não for vazio são efectuadas as seguintes operações.
           (set! dist1 (vector-ref (obtem-info-no info-nos (primeiro conj)) 1))   ;A variável 'dist1' assume o valor da distância do 1º elemento de 
           (set! no-aux (primeiro conj))             ;'conj'. A variável 'no-aux' assume o 1º elemento de 'conj'.
           (set! conj (resto conj))                  ;'conj' perde o 1º elemento.
           (set! no (if (< dist2 dist1)              ;Se 'dist2' for menor do que 'dist1', 'no' mantém-se. Caso contrário, 'no' assume  o  conteúdo
                        no                           ;de 'no-aux' actualizando-se para um nó com menor distância associada que os  nós  processados
                        no-aux))                     ;anteriormente.
           (set! dist2 (min dist1 dist2)))           ;'dist2' assume o valor mínimo entre 'dist1' e 'dist2'.   Assim,  as  variáveis 'no' e 'dist2'
    no))                                             ;apenas se alteram quando surge numa iteração um nó com menor distância associada.


;;Modificador

(define (actualiza-dist-ant-no! info-nos no int no-ant);Actualiza a distância e o nó anterior da info-nó, correspondente ao nó 'no', que pertence a
  (let ((info-no (obtem-info-no info-nos no)))       ;'info-nos'. É criada a variável 'info-no', que será a info-nó a actualizar.
    (vector-set! info-no 1 int)                      ;O valor da distância é alterado para 'int' e o conteúdo do nó anterior para 'no-ant'.
    (vector-set! info-no 2 no-ant))                  ;É devolvido 'info-nos' com a info-nó, correspondente ao nó 'no', actualizada.
  info-nos)



;;; CAMINHO-MAIS-CURTO ;;;

(define (caminho-mais-curto origem destino grafo)                            ;Determina o caminho mais curto entre 2 nós de um grafo rotulado.
  (if (not (grafo? grafo))                                                   ;Verifica a validade do 3º argumento.
      (error "caminho-mais-curto: O 3º argumento tem que ser do tipo grafo.")
      (let ((nos-G (nos grafo)))                                             ;;;;;;;;;;;;;;;;;;;Início do algoritmo de Dijkstra.;;;;;;;;;;;;;;;;;;;
        (cond ((not (or (pertence? origem nos-G)                             ;Caso o 3º argumento seja um grafo mas ambos os nós, origem e destino,
                        (pertence? destino nos-G)))                          ;não pertençam ao grafo,  é  gerado  um erro que esclarece o problema.
               (error "caminho-mais-curto: Os nós origem e destino têm que pertencer ao grafo."))
              ((not (and (pertence? origem nos-G)                            ;Caso o nó origem não pertença ao grafo,  é gerado um erro que explica
                         (pertence? destino nos-G)))                         ;o problema. Caso o nó destino não pertença ao grafo,  também é gerado
               (if (not (pertence? origem nos-G))                            ;um erro do mesmo tipo.
                   (error "caminho-mais-curto: O nó origem tem que pertencer ao grafo.")
                   (error "caminho-mais-curto: O nó destino tem que pertencer ao grafo.")))
              ((equal? origem destino) (insere (nova-lista) 0))              ;Caso os nós origem e destino sejam equivalentes,  então  a  distância
              ((or (lista-vazia? (vizinhos destino grafo))                   ;entre eles apenas pode ser zero, e não se avança no algoritmo.
                   (lista-vazia? (vizinhos origem grafo)))                   ;Caso o nó origem ou o nó destino não tenham vizinhos no grafo e, como
               (insere (nova-lista) +inf.0))                                 ;tal, não tenham rótulo,  então a distância  entre  eles  é  infinita.
              (else (let ((n +inf.0)                                         ;Caso  não  se  verifique  nenhuma  das  condições  anteriores, é dada
                          (info-inicial (actualiza-dist-ant-no!              ;continuação ao algoritmo de Dijkstra, que  foi  interrompido  para  a
                                         (cria-info-inicial nos-G) origem 0 null))   ;verificação das excepções.
                          (nova-dist +inf.0))
                      (while (pertence? destino nos-G)                       ;Utilizámos um ciclo  'while'  para  incorporar  o  'ENQUANTO',  tendo
                             (set! n (no<dist info-inicial nos-G))           ;atribuído valores iniciais às variáveis a utilizar no 'let' anterior.
                             (set! nos-G (retira-conj n nos-G))              ;Utilizámos um ciclo 'when' para incorporar o 'SE', pois apenas existe
                             (when (not (equal? n destino))                  ;uma condição, caso não se verifique o ciclo continua a iterar.
                               (dolist (v (interseccao (vizinhos n grafo)    ;Utilizámos um 'dolist' para incorporar o 'PARA CADA',  pois é o ciclo
                                                       nos-G))               ;mais adequado para a modificação dos elementos de um conjunto.
                                       (set! nova-dist                       ;Utilizámos a operação de atribuição 'set!' para incorporar '<-'.
                                             (+ (distancia-entre n v grafo) 
                                                (dist-info-no (obtem-info-no info-inicial n))))
                                       (when (< nova-dist (dist-info-no (obtem-info-no info-inicial v)));Mais uma vez, utilizámos  um  ciclo  'when'
                                         (actualiza-dist-ant-no! info-inicial v nova-dist n)))));pelos mesmos motivos anteriores.
                      (let ((anterior-a-n (ant-info-no (obtem-info-no info-inicial destino))))
                        (insere (do ((caminho (nova-lista) (if (lista-vazia? anterior-a-n)
                                                               (insere n caminho)
                                                               (insere (distancia-entre n anterior-a-n grafo) (insere n caminho))))
                                                                             ;Utilizámos o ciclo 'do' para criar a  lista  que  contém  os  nós  do
                                                                             ;caminho,  bem como,  entre cada dois nós,  a  distância  entre  eles.
                                                                             ;Caso 'n' (o nó) tenha nó anterior, então significa que é um  dos  nós
                                                                             ;que constituem o caminho mais curto ou o nó  destino (mas  não  o  nó
                                                                             ;origem) e, nesse caso, é inserida a distância  entre  o  nó  e  o  nó
                                                                             ;anterior, seguida do nó,  na variável 'caminho' que  tem  como  valor 
                                                                             ;inicial indefinido. Caso 'n' tenha nó anterior,  então  'n'  é  o  nó
                                                                             ;origem, pelo que é inserido no caminho para finalizar a lista.
                                     (n destino (ant-info-no (obtem-info-no info-inicial n))))
                                                                             ;A variável 'n' começa por corresponder ao nó destino,  evoluindo  por
                                                                             ;cada iteração de forma a corresponder ao nó anterior a 'n'.
                                  ((lista-vazia? n) caminho)                 ;É devolvido 'caminho', o 1º elemento do par, quando 'n' for vazio, ou
                                                                             ;seja, quando 'n' for o nó anterior ao nó origem.
                                  (set! anterior-a-n (ant-info-no (obtem-info-no info-inicial n))))
                                                                             ;'anterior-a-n' evolui por cada iteração de forma  a  que  seja  o  nó
                                                                             ;anterior a 'n'.
                                (dist-info-no (obtem-info-no info-inicial destino))))))))))
                                                                             ;Por fim, como 2º elemento  do  par,  temos  o  comprimento  total  do
                                                                             ;caminho, que se verifica ser a distância associada, em 'info-inicial',
                                                                             ;ao nó destino.
  

;;; CRIA-PROC-CAMINHOS-MAIS-CURTOS ;;;

(define (cria-proc-caminhos-mais-curtos grafo)                               ;Procedimento  com  estado  interno que determina o caminho mais curto
                                                                             ;entre   dois   nós   de   um   grafo,   tal   como   o   procedimento
  (if (not (grafo? grafo))                                                   ;'caminho-mais-curto', mas que, ao contrário deste, não  faz  cálculos
      (error "cria-proc-caminhos-mais-curtos: O argumento tem que ser do tipo grafo.")       ;repetidos da informação associada aos nós.
      
      (let ((nos-G (nos grafo))                                              ;Este 'let' serve para atribuir valores iniciais às variáveis a  serem
            (nos-G-aux (nos grafo))                                          ;utilizadas no algoritmo de Dijkstra modificado.   O 'let' surge antes
            (n +inf.0)                                                       ;do procedimento lambda a ser executado,  pois  estes valores iniciais
            (nova-dist +inf.0)                                               ;das variáveis apenas serão válidos para uma primeira determinação  de
            (nos-processados (nova-lista))                                   ;do caminho mais curto entre dois nós de um determinado grafo.  Após a
            (registo (nova-lista)))                                          ;primeira determinação,  estas  variáveis  tomarão outros valores mais
                                                                             ;adequados para a continuação do processamento dos  nós  restantes, na
                                                                             ;determinação de outro caminho mais curto para  um  destino  diferente.
                                                                             ;Isto apenas é válido caso o grafo se mantenha.
        (lambda (origem destino)                                             ;O procedimento lambda possui dois argumentos,  o  nó  origem  e  o nó
          (cond ((not (or (pertence? origem nos-G)                           ;destino, que têm que pertencer ao grafo dado.
                          (pertence? destino nos-G)))
                 (error "cria-proc-caminhos-mais-curtos: Os nós origem e destino têm que pertencer ao grafo."))
                ((not (and (pertence? origem nos-G)
                           (pertence? destino nos-G)))
                 (if (not (pertence? origem nos-G))
                     (error "cria-proc-caminhos-mais-curtos: O nó origem tem que pertencer ao grafo.")
                     (error "cria-proc-caminhos-mais-curtos: O nó destino tem que pertencer ao grafo.")))
                (else (cond ((equal? origem destino) (insere (nova-lista) 0));Tal como no procedimento 'caminho-mais-curto',  para o caso de  ambos
                            ((or (lista-vazia? (vizinhos destino grafo))     ;os nós, origem e destino,  serem iguais, é devolvido  um  par,  com o
                                 (lista-vazia? (vizinhos origem grafo)))     ;primeiro elemento vazio e o segundo elemento zero. E, caso um dos nós
                             (insere (nova-lista) +inf.0))                   ;um dos nós não esteja rotulado,  é  devolvido  um  par com o primeiro
                                                                             ;elemento vazio e o segundo elemento infinito.
                            (else (let ((info-inicial                        ;Neste 'let',  contido  no  lambda,   processado  em  cada execução do
                                         (if (or (lista-vazia? nos-processados) ;procedimento,  atribuímos à variável 'info-inicial',  caso seja  a
                                                              (not (equal? origem (no<dist registo nos-G))))  ;primeira  execução  do  procedimento
                                                          (begin (set! nos-G-aux (nos grafo))  ;para um determinado grafo ou caso  seja  a  segunda 
                                                                 (set! nos-processados (nova-lista))  ;execução do procedimento para um mesmo grafo
                                                                 (actualiza-dist-ant-no!     ;mas  uma  origem  diferente,  o  mesmo  conteúdo  que
                                                                  (cria-info-inicial nos-G) origem 0 null))  ;lhe  era  atribuído  no  procedimento
                                                          registo)))         ;'caminho-mais-curto'. Mas, caso se trate do mesmo grafo e do mesmo nó
                                                                             ;origem processados anteriormente, a variável  info-inicial  mantém  o
                                                                             ;seu conteúdo. Somos obrigados a calcular  a  'info-inicial' de novo a
                                                                             ;partir do zero para uma origem diferente pois, segundo o algoritmo de
                                                                             ;Dijkstra,  o  valor  da  distância associada ao nó origem tem que ser
                                                                             ;zero, e não faz sentido calcular para valores diferentes.
                                                                             ;Se a lista 'nos-processados' for vazia,  significa que o procedimento
                                                                             ;já foi executado para esse mesmo gráfico e origem.  Caso  o nó origem
                                                                             ;não  corresponda  ao  nó  com menor distância associada no registo da
                                                                             ;execução anterior, significa que o nó origem a ser processado é outro
                                                                             ;e, como tal, voltamos à estaca zero.
                                    (while (pertence? destino nos-G-aux)
                                           (set! n (no<dist info-inicial nos-G-aux))
                                           (set! nos-G-aux (retira-conj n nos-G-aux))
                                           (when (not (equal? n destino))
                                             (set! nos-processados (insere n nos-processados)) ;É inserido este processo no cerne  do  algoritmo de
                                                                             ;Dijkstra para criar uma lista com os  nós  processados,  que servirá,
                                                                             ;numa próxima execução do procedimento,  para  um  mesmo  grafo  e uma
                                                                             ;uma mesma origem,  dar  uma  instrução  (que é dada no 'let' anterior)
                                                                             ;para que a variável 'info-inicial' se mantenha,  e  assim continuar a
                                                                             ;processar os nós que restam.
                                             (dolist (v (interseccao (vizinhos n grafo)
                                                                     nos-G-aux))
                                                     (set! nova-dist
                                                           (+ (distancia-entre n v grafo) 
                                                              (dist-info-no (obtem-info-no info-inicial n))))
                                                     (when (< nova-dist (dist-info-no (obtem-info-no info-inicial v)))
                                                       (actualiza-dist-ant-no! info-inicial v nova-dist n)))))
                                    
                                    (begin (set! registo info-inicial)       ;Inserimos este processo no final do algoritmo de Dijkstra  modificado
                                           (set! nos-G-aux (insere destino nos-G-aux))) ;para que,  após uma execução do procedimento,  a  variável
                                                                             ;'registo' fique com o conteúdo da variável  'info-inicial',  evitando
                                                                             ;assim fazer cálculos repetidos numa próxima  execução do procedimento
                                                                             ;para um mesmo grafo e uma mesma origem e,  também para que a variável
                                                                             ;'nos-G-aux', que representa os nós que faltam processar,  contenha  o
                                                                             ;nó destino, que é removido no ciclo 'while' anterior,  mas  faz falta
                                                                             ;numa próxima execução do programa, para um mesmo grafo  e  uma  mesma
                                                                             ;origem, pois o nó destino ainda não chegou a ser processado.
                                    
                                    (let ((anterior-a-n (ant-info-no (obtem-info-no info-inicial destino))))
                                      (insere (do ((caminho (nova-lista) (if (lista-vazia? anterior-a-n)
                                                                             (insere n caminho)
                                                                             (insere (distancia-entre n anterior-a-n grafo)
                                                                                     (insere n caminho))))
                                                   (n destino (ant-info-no (obtem-info-no info-inicial n))))
                                                ((lista-vazia? n) caminho)
                                                (set! anterior-a-n (ant-info-no (obtem-info-no info-inicial n))))
                                              (dist-info-no (obtem-info-no info-inicial destino)))))))))))))
