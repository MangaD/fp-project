# FP Project

**Course:** [Foundations of Programming](https://fenix.tecnico.ulisboa.pt/disciplinas/FP3/2011-2012/1-semestre)  
**University:** [Instituto Superior Técnico](https://tecnico.ulisboa.pt/)  
**Academic year:** 2011-12

### Team

- 73824 — Daniel Lascas ([daniel.lascas@tecnico.ulisboa.pt](mailto:daniel.lascas@tecnico.ulisboa.pt))
- 73891 — David Gonçalves  ([david.s.goncalves@tecnico.ulisboa.pt](mailto:david.s.goncalves@tecnico.ulisboa.pt))
- 74005 — Ricardo Moura ([ricardo.de.moura@tecnico.ulisboa.pt](mailto:ricardo.de.moura@tecnico.ulisboa.pt))

Note: This project has been translated to English on 20 October 2020 by David Gonçalves, who also took the liberty to re-implement the code of part 1, although the original is still available in Portuguese.

## Scheme

>  Scheme is a programming language that supports multiple paradigms, including functional programming and imperative programming, and is one of the two main dialects of [Lisp](https://wiki.archlinux.org/index.php/Lisp). Unlike [Common Lisp](https://wiki.archlinux.org/index.php/Common_Lisp), the other main dialect, Scheme follows a minimalist design philosophy specifying a small standard core with powerful tools for language extension.
> -- <cite>https://wiki.archlinux.org/index.php/Scheme</cite>

## Assignment

- Part 1: [Portuguese original](part_1/projecto-parte1-v2.pdf) | [English](part_1/Assignment.md)

- Part 2: [Portuguese original](part_2/projecto-parte2-v2.pdf) | [English](part_2/Assignment.md)
- Part 3: [Portuguese original](part_3/projecto-parte3.pdf) | [English](part_3/Assignment.md)

This project is divided into three parts. The goal of the first part is to get acquainted with the Scheme language and the recursion method. The goal of the second part of the project is to define and implement some abstract types of information that will be used in the third and final part, which consists in the implementation of Dijkstra's algorithm, with some variations.

## Environment Setup

Arch Linux:

```sh
sudo pacman -Sy racket
```

## Run scripts

Part 1:

```sh
# Open file 'part_1/part1.scm' with DrRacket, click Run. Then the functions can be tested as exemplified below.
(prime? 6)
(nth-prime 45)
```

Part 2:

```sh
# Open file 'part_2/tests-part2.scm' with DrRacket, click Run to run the tests. The code being tested is in the file 'part_2/part2.scm'
```

Part 3:

```sh
# Open file 'part_3/tests-part3.scm' with DrRacket, click Run to run the tests. The code being tested is in the file 'part_3/part3.scm'
```