; David Gonçalves - 2020 remake

#lang scheme

(provide (all-defined-out))

; LIST OPERATIONS

(define (elem=? e1 e2)
  (if (and (edge? e1) (edge? e2))
      (edges=? e1 e2)
      (equal? e1 e2)))

(define (new-list)
  '())

(define (insert elem lst)
  (cons elem lst))

(define (firstEl lst)
  (if (null? lst)
      (error "firstEl: the list has no elements")
      (car lst)))

(define (restElms lst)
  (if (null? lst)
      (error "restElms: the list has no elements")
      (cdr lst)))

(define (list? x)
  (cond ((null? x) #t)
        ((pair? x) (list? (cdr x)))
        (else #f)))

(define (empty-list? lst) 
  (null? lst))

(define (lists=? lst1 lst2)
  (cond ((null? lst1) (null? lst2))
        ((null? lst2) #f)
        ((elem=? (car lst1) (car lst2)) 
         (lists=? (cdr lst1) (cdr lst2)))
        (else #f)))

(define (list-length lst)
  (if (empty-list? lst)
      0
      (+ 1 (list-length (restElms lst)))))

(define (all-elements-list-satisfy? lst test)
  (cond ((empty-list? lst) #t)
        ((test (firstEl lst))
         (all-elements-list-satisfy? (restElms lst) test))
        (else #f)))

; SET OPERATIONS

(define (new-set) (new-list))

(define (insert-set e c)
  (define (insert-set-aux e c)
    (cond ((empty-list? c) (insert e c))
          ((elem=? e (firstEl c)) c)
          (else (insert (firstEl c) 
                        (insert-set e (restElms c))))))
  (if (set? c)
      (insert-set-aux e c)
      (error "insert-set: the second argument must be a set.")))

(define (make-set . elements)
  (define (make-set-aux  elements)
    (if (empty-list? elements) 
        (new-set)
        (insert-set (firstEl elements) 
                     (make-set-aux (restElms elements)))))
  (make-set-aux elements))

(define (remove-set e c)
  (cond ((empty-list? c) (new-list))
        ((elem=? e (firstEl c)) (restElms c))
        (else (insert (firstEl c) 
                      (remove-set e (restElms c))))))

(define (set? x)
  (define (has-no-repeated-elements? x)
    (cond ((empty-list? x) #t)
          ((belongs? (firstEl x) (restElms x)) #f)
          (else (has-no-repeated-elements? 
                  (restElms x)))))
  (and (list? x) 
       (has-no-repeated-elements? x)))

(define (empty-set? c)
  (empty-list? c))

(define (sets=? c1 c2)
  (cond ((empty-list? c1) (empty-list? c2))
        ((empty-list? c2) #f)
        ((belongs? (firstEl c1)  c2)
         (sets=? (restElms c1) 
                      (remove-set (firstEl c1) c2)))
        (else #f)))

(define (belongs? e c)
  (cond ((empty-list? c) #f)
        ((elem=? e (firstEl c)) #t)
        (else (belongs? e (restElms c)))))

(define (all-set-elements-satisfy? c test)
  (all-elements-list-satisfy? c test))

(define (set->list conj)
  conj)

(define (intersect c1 c2)
  (cond ((empty-list? c1) (new-list))
        ((belongs? (firstEl c1) c2)
         (insert (firstEl c1)
                 (intersect (restElms c1) c2)))
        (else (intersect (restElms c1) c2))))

; EDGE OPERATIONS

(define (make-edge n1 n2 rot)
  (if (integer? rot)
      (cons (make-set n1 n2) rot)
      (error "make-edge: the third argument must be an integer")))

(define (edge-extremities r)
  (car r))

(define (edge-label r)
  (cdr r))

(define (edge-another-node n r)
  (firstEl (set->list
             (remove-set n (edge-extremities r)))))

(define (edge? x)
  (and (pair? x)
       (set? (car x))
       (integer? (cdr x))))

(define (edges=? r1 r2)
  (and (= (edge-label r1) (edge-label r2))
       (sets=? (edge-extremities r1) (edge-extremities r2))))

(define (edge-extremity? n r)
  (belongs? n (edge-extremities r)))

; GRAPH OPERATIONS

(define (make-graph set-nodes set-edges)
  (if (and (set? set-nodes) 
           (set? set-edges) 
           (all-set-elements-satisfy? set-edges edge?))
      (cons set-nodes set-edges)
      (error "make-graph: the 1st argument must be a set of nodes, and the second argument must be a set of edges.")))

(define (nodes G)
  (car G))

(define (edges G)
  (cdr G))

(define (neighbors n G)
  (define (neighbors-aux n lst-edges)
    (cond ((empty-list? lst-edges) (new-set))
          ((belongs? n (edge-extremities (firstEl lst-edges)))
           (insert-set (edge-another-node n (firstEl lst-edges))
                        (neighbors-aux n (restElms lst-edges))))
          (else (neighbors-aux n (restElms lst-edges)))))
  
  (neighbors-aux n (set->list (edges G))))

(define (distance-between n1 n2 G)
  
  (define (distance-between-aux n1 n2 lst-edges)
    (cond ((empty-list? lst-edges)
           (error "distance-between: there is no edge between the given nodes."))
          ((sets=? (edge-extremities (firstEl lst-edges))
                        (make-set n1 n2))
           (edge-label (firstEl lst-edges)))
          (else (distance-between-aux n1 n2 (restElms lst-edges)))))
  
  (distance-between-aux n1 n2 (set->list (edges G))))

(define (graph? u)
  (and (pair? u)
       (set? (car u))
       (set? (cdr u))
       (all-set-elements-satisfy? (cdr u) edge?)))

(define (graphs=? g1 g2)
  (and (sets=? (nodes g1) (nodes g2))
       (sets=? (edges g1) (edges g2))))


