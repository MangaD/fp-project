#lang scheme
(require (lib "misc.ss" "swindle")
         (lib "list.ss")
         (lib "lset.ss" "srfi" "1"))

(define (make-test name form error?)
  (cons error? (cons name form)))

(define (test-name test)
  (cadr test))

(define (form-test test)
  (cddr test))

(define (test-error? test)
  (car test))


;; Definição dos test
(define (make-tests)
  (list 
   (make-test "test insert-set 01"
               '(equal?
                 (insert-set 1 5 )
                 'error)
               #t)
   (make-test "test make-set 01"
               '(sets=?
                 (make-set 1 2 )
                 (insert-set 2 (insert-set 1 (new-set))))
               #f)
   (make-test "test remove-set 01"
               '(sets=?
                 (remove-set 1 (make-set 1 2 3) )
                 (make-set 2 3))
               #f)
   (make-test "test remove-set 02"
               '(sets=?
                 (remove-set 1 (make-set 2 3) )
                 (make-set 2 3))
               #f)
   (make-test "test set? 01"
               '(equal?
                 (set? (new-set) )
                 #t)
               #f)
   (make-test "test set? 02"
               '(equal?
                 (set? (make-set 1 2 3) )
                 #t)
               #f)
   (make-test "test set? 03"
               '(equal?
                 (set? (insert-set 1 (make-set 1 2 3)) )
                 #t)
               #f)
   (make-test "test set? 04"
               '(equal?
                 (set? 1 )
                 #f)
               #f)
   (make-test "test empty-set? 01"
               '(equal?
                 (empty-set? (new-set) )
                 #t)
               #f)
   (make-test "test empty-set? 02"
               '(equal?
                 (empty-set? (make-set 1 2 3) )
                 #f)
               #f)
   (make-test "test empty-set? 03"
               '(equal?
                 (empty-set? (insert-set 1 (make-set 1 2 3)) )
                 #f)
               #f)
   (make-test "test sets=? 01"
               '(equal?
                 (sets=? (make-set 1 2 3) (make-set 3 2 1) )
                 #t)
               #f)
   (make-test "test sets=? 02"
               '(equal?
                 (sets=? (make-set 1 2 3) (make-set 3 2 1 3 2) )
                 #t)
               #f)
   (make-test "test sets=? 03"
               '(equal?
                 (sets=? (make-set 1 2 3) (insert-set 1 (make-set 1 2 3)) )
                 #t)
               #f)
   (make-test "test sets=? 04"
               '(equal?
                 (sets=? (make-set 1 2 3) (insert-set 6 (make-set 1 2 3)) )
                 #f)
               #f)
   (make-test "test belongs? 01"
               '(equal?
                 (belongs? 3 (new-set) )
                 #f)
               #f)
   (make-test "test belongs? 02"
               '(equal?
                 (belongs? 5 (make-set 1 2 3) )
                 #f)
               #f)
   (make-test "test belongs? 03"
               '(equal?
                 (belongs? 4 (insert-set 1 (make-set 1 2 3)) )
                 #f)
               #f)
   (make-test "test belongs? 04"
               '(equal?
                 (belongs? 2 (make-set 1 2 3) )
                 #t)
               #f)
   (make-test "test belongs? 05"
               '(equal?
                 (belongs? 2 (insert-set 1 (make-set 1 2 3)) )
                 #t)
               #f)
   (make-test "test all-set-elements-satisfy? 01"
               '(equal?
                 (all-set-elements-satisfy? (make-set 1 5 3) odd? )
                 #t)
               #f)
   (make-test "test all-set-elements-satisfy? 02"
               '(equal?
                 (all-set-elements-satisfy? (make-set 1 2 5 3) odd? )
                 #f)
               #f)
   (make-test "test set->list 01"
               '(equal?
                 (set->list (new-set) )
                 '())
               #f)
   (make-test "test set->list 02"
               '(let ((lst1 (set->list (make-set 1 2 3)) )
                      (lst2 (list 1 2 3)))
                  (and (lset= equal? lst1 lst2)
                       (= (length lst1) (length lst2))))
               #f)
   (make-test "test set->list 03"
               '(let ((lst1 (set->list (insert-set 1 (make-set 1 2 3)) ))
                      (lst2 (list 1 2 3)))
                  (and (lset= equal? lst1 lst2)
                       (= (length lst1) (length lst2))))
               #f)
   (make-test "test intersect 01"
               '(sets=?
                 (intersect (make-set 1 2 3) (make-set 3 2 1) )
                 (make-set 1 2 3))
               #f)
   (make-test "test intersect 02"
               '(sets=?
                 (intersect (make-set 1 2 3) (make-set 3 1) )
                 (make-set 1 3))
               #f)
   (make-test "test intersect 03"
               '(sets=?
                 (intersect (make-set 1 2 3) (make-set 3 1 4) )
                 (make-set 1 3))
               #f)
   (make-test "test make-edge 01"
               '(equal?
                 (make-edge 'A 'B 7.9 )
                 'error)
               #t)
   (make-test "test edge-extremities 01"
               '(sets=?
                 (edge-extremities (make-edge 'A 'B 7) )
                 (make-set 'A 'B))
               #f)
   (make-test "test edge-label 01"
               '(equal?
                 (edge-label (make-edge 'A 'B 7) )
                 7)
               #f)
   (make-test "test edge-another-node 01"
               '(equal?
                 (edge-another-node 'A (make-edge 'A 'B 7) )
                 'B)
               #f)
   (make-test "test edge? 01"
               '(equal?
                 (edge? (make-edge 'A 'B 7) )
                 #t)
               #f)
   (make-test "test edge? 02"
               '(equal?
                 (edge? 3 )
                 #f)
               #f)
   (make-test "test edges=? 01"
               '(equal?
                 (edges=? (make-edge 'A 'B 7) (make-edge 'B 'A 7) )
                 #t)
               #f)
   (make-test "test edges=? 02"
               '(equal?
                 (edges=? (make-edge 'A 'B 7) (make-edge 'A 'B 5) )
                 #f)
               #f)
   (make-test "test edge-extremity? 01"
               '(equal?
                 (edge-extremity? 'A (make-edge 'A 'B 7) )
                 #t)
               #f)
   (make-test "test edge-extremity? 02"
               '(equal?
                 (edge-extremity? 'B (make-edge 'A 'B 7) )
                 #t)
               #f)
   (make-test "test edge-extremity? 03"
               '(equal?
                 (edge-extremity? 'C (make-edge 'A 'B 7) )
                 #f)
               #f)
   (make-test "test make-graph 01"
               '(equal?
                 (make-graph (make-set 'A 'B 'C) (make-set 4 (make-edge 'A 'C 7)) )
                 'error)
               #t)
   (make-test "test nodes 01"
               '(sets=?
                 (nodes (make-graph (new-set) (new-set)) )
                 (make-set))
               #f)
   (make-test "test nodes 02"
               '(sets=?
                 (nodes (make-graph (make-set 'A 'B) (new-set)) )
                 (make-set 'A 'B))
               #f)
   (make-test "test edges 01"
               '(sets=?
                 (edges (make-graph (new-set) (new-set)) )
                 (make-set))
               #f)
   (make-test "test edges 02"
               '(sets=?
                 (edges (make-graph (new-set) (make-set (make-edge 'A 'B 7) (make-edge 'A 'C 7))) )
                 (make-set (make-edge 'A 'B 7) (make-edge 'A 'C 7)))
               #f)
   (make-test "test neighbors 01"
               '(sets=?
                 (neighbors 'A (make-graph (make-set 'A 'B 'C 'D) (make-set (make-edge 'A 'B 7) (make-edge 'A 'C 5))) )
                 (make-set 'B 'C))
               #f)
   (make-test "test neighbors 02"
               '(sets=?
                 (neighbors 'D (make-graph (make-set 'A 'B 'C 'D) (make-set (make-edge 'A 'B 7) (make-edge 'A 'C 5))) )
                 (new-set))
               #f)
   (make-test "test distance-between 01"
               '(equal?
                 (distance-between 'A 'B (make-graph (make-set 'A 'B 'C 'D) (make-set (make-edge 'A 'B 7) (make-edge 'A 'C 5))) )
                 7)
               #f)
   (make-test "test graph? 01"
               '(equal?
                 (graph? (make-graph (new-set) (new-set)) )
                 #t)
               #f)
   (make-test "test graph? 02"
               '(equal?
                 (graph? (make-graph (make-set 'A 'B 'C 'D) (make-set (make-edge 'A 'B 7) (make-edge 'A 'C 5))) )
                 #t)
               #f)
   (make-test "test graph? 03"
               '(equal?
                 (graph? 4 )
                 #f)
               #f)
   (make-test "test graphs=? 01"
               '(equal?
                 (graphs=? (make-graph (make-set 'A 'B 'C 'D) (make-set (make-edge 'A 'B 7) (make-edge 'A 'C 5))) (make-graph (make-set 'A 'B 'C 'D) (make-set (make-edge 'A 'B 7) (make-edge 'A 'C 5))) )
                 #t)
               #f)
   (make-test "test graphs=? 02"
               '(equal?
                 (graphs=? (make-graph (make-set 'A 'B 'C 'D) (make-set (make-edge 'A 'B 7) (make-edge 'A 'C 5))) (make-graph (make-set 'A 'B 'C) (make-set (make-edge 'A 'B 7) (make-edge 'A 'C 5))) )
                 #f)
               #f)
   ))


(define (testing tests)
  (dolist (test tests)
          ; In MIT Scheme, eval requires the environment as 2nd argument
          ; In Racket it takes only one argument
          ; (eval (form-test test) user-initial-environment)
          (let ((res (no-errors* (eval (form-test test)))))
            (newline)
            (display (test-name test))
            (cond ((struct? res) (if (test-error? test)
                                     (display " - Pass.")
                                     (display " - FAIL.")))
                  ((test-error? test) (display " - FAIL."))
                  (res (display " - Pass."))
                  (else (display " - FAIL."))))))

(parameterize ([current-namespace (make-base-empty-namespace)])
  (namespace-require 'scheme)
  (namespace-require (string-append "part2.scm"))
  (eval '(require (lib "misc.ss" "swindle")
                  (lib "list.ss")
                  (lib "lset.ss" "srfi" "1")
                  (lib "trace.ss")))
  (testing (make-tests)))


