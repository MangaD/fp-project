# Foundations of Programming

## Project - Part 2

15 November 2011

The goal of the second part of the project is to define and implement some abstract types of information that will be used in the final project.

The types implemented on this second part will be used not only by the program to develop in this part, as well as other programs, namely the program of automatic evaluation. Therefore, you should scrupulously respect the definitions that are given ahead, both in the names of procedures, as well as in the number and order of the formal parameters.

Besides the types described below, you must include in your code the implementation of the simplified list type presented in the [book](http://istpress.tecnico.ulisboa.pt/en/node/339); you need not, however, to include the abstract definition of the simplified list type in your report.

In order to not unnecessarily overload the code, only the constructors of each type must verify the validity of the arguments they receive.

## 1. The 'set' type

The 'set' type should provide the following basic operations.

- Constructors.
  - `new-set`: {} -> *set*
    `new-set()` returns an empty set.
    
  - `insert-set`: *universal* &times; *set* -> *set*
    `insert-set(e,c)` returns the resulting set from inserting element *e* in set *c*. If *e* already belongs to *c*, `insert-set(e,c)` returns `c`.
    
  - `make-set`: *universal<sup>n</sup>* -> *set*
  
    *make-set(e<sub>1</sub>, ..., e<sub>n</sub>)* returns the set of elements *(e<sub>1</sub>, ..., e<sub>n</sub>)*. For example,
  
    `make-set(1, 2, 3, 4, 2, 2)` returns the set `{1, 2, 3, 4}`.
  
- Selector

  - `remove-set`: *universal* &times; *set* -> *set*
    `remove-set(e,c)` returns the resulting set from removing element *e* from set *c*. If *e* does not belong to *c*, `remove-set(e,c)` returns `c`.

- Recognizer

  - `set?`: *universal* -> *logical*
    `set?(u)` returns `true` is `u` is a set, and `false` otherwise.
  - `empty-set?`: *set* -> *logical*
    `empty-set?(c)` returns `true` is `c` is empty, and `false` otherwise.

- Tests

  - `sets=?`: *set* &times; *set* -> *logical*
    *sets=?(c<sub>1</sub>, c<sub>2</sub>)* returns `true` if c<sub>1</sub> and c<sub>2</sub> are equal sets, and `false` otherwise.
  - `belongs?`: *universal* &times; *set* -> *logical*
    `belongs?(e, c)` returns `true` if is `u` is a set, and `false` otherwise.
  - `all-set-elements-satisfy?` : *set* &times; *predicate* -> *logical*
    `all-set-elements-satisfy?(c, p)` returns `true` if all elements in the set *c* satisfy the predicate *p*, and `false` otherwise.

- Transformer

  - `set->list`: *set* -> *list*
    `set->list(c)` returns a list consisting on the elements of set *c*. The order of the elements in the list is not relevant.

- Additional operation

  - `intersect`: *set* &times; *set* -> *set*
    *intersect(c<sub>1</sub>, c<sub>2</sub>)* returns the intersection set of c<sub>1</sub> and c<sub>2</sub>, that is, the set made up by all elements that belong simultaneously to  c<sub>1</sub> and c<sub>2</sub>.

## 2. The types 'graph' and 'edge'

A labelled graph is a structured type made up by a set of nodes and a set of edges between nodes. Each edge has a label. For example, the graph below

![graph_example](graph_example.png)

is made up by the set of nodes `{A, B, C, D, E, F}`, and by the set of edges `{(A B 7), (A C 8), (A F 14), (B C 10), (B D 15), (C D 11), (C F 2), (D E 6), (E F 9)}`.

Even though the nodes may have associated information, we consider that a node is equivalent to its identifier which can be of any type.

### 2.1. The 'edge' type

An edge is made up by a set of two nodes, the *extremities* of the edge, and by an integer, the *label* of the edge. The 'edge' type must provide the following basic operations:

- Constructor
  
  - `make-edge`: *node* &times; *node* &times; *integer* -> *edge*
  - *make-edge(n<sub>1</sub>, n<sub>2</sub>, r)* returns the edge with the extremities n<sub>1</sub> and n<sub>2</sub>, and label *r*.
  
- Selectors

  - `edge-extremities`: *edge* -> *set of nodes*
    `edge-extremities(r)` returns the set containing the extremities of edge *r*.
  - `edge-label` : *edge* -> *integer*
    `edge-label(r)` returns the label of edge *r*.
  - `edge-another-node` : *node* &times; *edge* -> *node*
    `edge-another-node(n, r)` with *n* being one of the edge's extremities, it returns the other extremity of the edge *r*.

- Recognizer

  - `edge?` : *universal* -> *logical*
    `edge?(u)` returns `true` if *u* is an edge, and false otherwise.

- Tests

  - `edges=?` : *edge* &times; *edge* -> *logical*
    *edges=?(r<sub>1</sub>, r<sub>2</sub>)* returns `true` if r<sub>1</sub> and r<sub>2</sub> are equal edges, and `false` otherwise. Two edges are considered equal if they have the same extremities and the same label. Note that no order exists between the extremities, therefore the following axiom should be valid:
    *edges=?(make-edge(n<sub>1</sub>, n<sub>2</sub>, r), make-edge(n<sub>2</sub>, n<sub>1</sub>, r)) = true*
  - `edge-extremity?` : *node* &times; *edge* -> logical
    `edge-extremity?(n, r)` returns `true` if *n* is an extremity of edge *r*, and false otherwise.

### 2.1. The 'graph' type

The 'graph' type must provide the following basic operations:

- Constructor
  
  - `make-graph`: *set of nodes* &times; *set of edges* -> *graph*
    *make-graph(n, r)* returns the graph whose set of nodes is *n* and set of edges is *r*.
  
- Selectors

  - `nodes`: *graph* -> *set of nodes*
    `nodes(g)` returns the set of nodes of graph *g*.
  - `edges`: *graph* -> *set of edges*
    `edges(g)` returns the set of edges of graph *g*.
  - `neighbors`: *node* &times; *graph* -> *set of nodes*
    `neighbors(n, g)` returns the set of nodes of *g* connected by an edge to node *n*. For example, the neighbors of node *A* of the figure above are the nodes of the set `{B, C, F}`.
  - `distance-between` : *node* &times; *node* &times; *graph* -> *integer*
    *distance-between(n<sub>1</sub>, n<sub>2</sub>, g)*  returns the label of the edge with extremities n<sub>1</sub> and n<sub>2</sub> if it exists, otherwise the returned value is undefined.

- Recognizer

  - `graph?` : *universal* -> *logical*
    `graph?(u)` returns `true` if *u* is a graph, and false otherwise.

- Tests

  - `graphs=?` : *graph* &times; *graph* -> *logical*
    *graphs=?(g<sub>1</sub>, g<sub>2</sub>)* returns `true` if g<sub>1</sub> and g<sub>2</sub> are equal graphs, and `false` otherwise.
  

