# Foundations of Programming

## Project - Part 3

2 December 2011

## Shortest paths in graphs

Graphs are important structures of information in computer science. One of the tasks that is frequently done on graphs is determining the shortest path between two nodes, for which there are several algorithms.

The goal of this project is to develop a program that determines the shortest path between two nodes of a graph, using the Dijkstra's algorithm.

## 1. Dijkstra's algorithm

Dijkstra's algorithm determines the shortest path between two nodes of a labelled graph. Consider the graph of figure 1. In this graph, there exist two paths between the nodes *A* and *C*: `[A, C]`, of length 14, and `[A, B, C]`, of length 10. Therefore, the shortest path between *A* and *C* is the path `[A, B, C]`

During its execution, the algorithm associates some information to the nodes of the graph. The information associated to a node consists of:

- a *distance*, an integer,
- a *previous node*, a node.

Given a node *n*, we use `dist(n)` and `prev(n)` to represent the distance and the previous node associated to node *n*, respectively. The distance associated with every node is initialised to &infin;, except the origin node, which is initialised to zero. The previous nodes associated with a node are initially undefined.

![figure 1](figure_1.png)
*Figure 1: Example of a graph.*

![figure 2](figure_2.png)
*Figure 2: Example of a graph.*

| n | A | B | C | D | E | F | G |
|-|-|-|-|-|-|-|-|
| dist(n) | 0 | &infin; | &infin; | &infin; | &infin; | &infin; | &infin; |
| prev(n) | ? | ? | ? | ? | ? | ? | ? |
*Table 1: Initial values.*

Considering the graph of figure 2 and assuming that the origin node is *A*, the values of `dist(n)` and `prev(n)` are those shown in table 1 (the "undefined" value is represented with the symbol "?").

The algorithm's pseudo-code is shown below. The symbol "<-" represents the assignment operation.

```
shortest-path(origin, destiny, graph)

nodes-G <- nodes(graph)
Initialize the information associated with each node of nodes-G
WHILE belongs?(destiny, nodes-G)
	n <- node of nodes-G with shortest distance associated
	nodes-G <- remove-set(n, nodes-G)
	IF n is not the destiny node
		FOR EACH v of the set intersect(neighbors(n, graph), nodes-G)
			new-dist <- distance-between(n, v, graph) + dist(n)
			IF new-dist < dist(v)
				dist(v) <- new-dist
				prev(v) <- n
			END IF
		END FOR EACH
	END IF
END WHILE
path <- new-list()
n <- destiny
WHILE n is not undefined
	path <- insert(n, path)
	n <- prev(n)
END WHILE
RETURN path
```

| n | A | B | C | D | E | F | G |
|-|-|-|-|-|-|-|-|
| dist(n) | 0 | 9 | 7 | &infin; | &infin; | 14 | &infin; |
| prev(n) | ? | A | A | ? | ? | A | ? |
*Table 2: Values after processing node A.*


| n | A | B | C | D | E | F | G |
|-|-|-|-|-|-|-|-|
| dist(n) | 0 | 9 | 7 | 22 | &infin; | 14 | &infin; |
| prev(n) | ? | A | A | C | ? | A | ? |
*Table 3: Values after processing node C.*

| n | A | B | C | D | E | F | G |
|-|-|-|-|-|-|-|-|
| dist(n) | 0 | 9 | 7 | 20 | &infin; | 11 | &infin; |
| prev(n) | ? | A | A | B | ? | B | ? |
*Table 4: Values after processing node B.*

In tables 2, 3 and 4 it is shown, for each iteration of the `WHILE` cycle, the values of distance and the previous node associated with each node of the graph, assuming that the graph is the one of figure 2, and that the origin node is A and destiny node is F. In the last iteration F is selected, and because it is the destiny node it is not processed, ending the cycle. As such, the final values are the ones shown in table 4. From these values it is built the shorted path between nodes *A* and *F*: `(A B F) `.

## 2. Program to develop

You should develop a procedure, `(shortest-path origin destiny graph)`, corresponding to an implementation of the Dijkstra's algorithm, that, given a node of origin, a destiny node, and a graph, determines the shortest path between the origin and the destiny, considering the given graph. Your procedure should validate the arguments that it receives, that is, verify if the third argument is a graph, and if the first two arguments correspond to nodes of that graph.

Besides calculating the shortest path between two nodes of a graph, your procedure should also show the distance between adjacent nodes of the path, as well as the total distance. The procedure `shortest-path` must return a pair in which:

- The first element of the pair is a list that contains the nodes of the path, as well as, between every two nodes, the distance between them.
- The second element of the pair is the total length of the path.

Using your procedure, and considering that the graph is the one of figure 2, it should be possible to obtain the following interaction:

```scheme
> (shortest-path 'A 'F graph)
((A 9 B 2 F) . 11)
> (shortest-path 'A 'E graph)
((A 9 B 2 F 9 E) . 20)
> (shortest-path 'E 'E graph)
(() . 0)
> (shortest-path 'A 'G graph)
(() . +inf.0)
```

The result of the last interaction indicates that there is no path between nodes *A* and *G*. The primitive constant `+inf.0` represents the value &infin;.

In the following section are defined the information types to develop before implementing the Dijkstra's algorithm.

## 3. Types of information

Besides the types of the second part of the project, you should implement the types specified below.

In order not to unnecessarily overload the code, only the constructors of each type should verify the validity of the arguments that they receive.

### 3.1. The type 'node-info'

The type 'node-info' is used for representing the information associated with a node by the Dijkstra's algorithm. An element of type 'node-info' is made of a node *n*, by the distance associated with *n*, and by the previous node associated with *n*. Note that each column of the tables presented in section 1 correspond to an element of type 'node-info'. This type should provide the following basic operations:

- Constructor:
  - `new-node-info` : *node* -> *node-info*
    `new-node-info(n)` returns the element of type `node-info` whose node is *n*, whose distance is &infin;, and whose previous node is "undefined" (represent this value with `null`).
- Selectors:
  - `node-node-info`: *node-info* -> *node*
    `node-node-info(i)` returns the node of *i*.
  - `dist-node-info`: *node-info* -> *integer*
    `dist-node-info(i)` returns the distance of *i*.
  - `prev-node-info`: *node-info* -> *node*
    `prev-node-info(i)` returns the previous node of *i*.
- Modifiers:
  - `modify-dist!`: *node-info* &times; *integer* -> *node-info*
    `modify-dist!(i, d)` changes the distance of *i* to *d*. Returns *i* with the new distance.
  - `modify-prev!`: *node-info* &times; *node* -> *node-info*
    `modify-prev!(i, n)` changes the previous node of *i* to *n*. Returns *i* with the new previous node.

### 3.2. The type 'nodes-info'

The type 'nodes-info' is used to represent the information associated with the nodes of a graph by the Dijkstra's algorithm. An element of the type `nodes-info` is composed by a collection of elements of the type `node-info`. Note that each table presented in section 1 corresponds to an element of type 'nodes-info'. This type should provide the following basic operations:

- Constructor:
  - `create-initial-info`: *set of nodes* -> *nodes-info*
    `create-initial-info(c)` returns the collection of elements of type `node-info` obtained through executing `new-node-info` to each node of set *c*.
- Selectors:
  -  `get-node-info`: *nodes-info* &times; *node* -> *node-info*
     `get-node-info(i, n)` returns the element of *i* whose node is *n*.
  - `node<dist`: *nodes-info* &times; *set of nodes* -> *node*
    `node<dist(i, c)`: returns the node of set *c* with the shortest distance associated with *i*.
- Modifier:
  - `update-dist-prev-node!`: *nodes-info* &times; *node* &times; *integer* &times; *node* -> *nodes-info*
    `update-dist-prev-node!(i, n, d, a)` changes the distance and the previous node associates to node *n* in *i*, for *d* and for *a* respectively. Returns *i* with the updated information associated to node *n*.

For a better understanding of the functioning of these basic operations, a few examples are shown below.

The operation `create-initial-info({A, B, C, D, E, F, G})` returns an element of the type `nodes-info` with the following content:

| A | B | C | D | E | F | G |
|-|-|-|-|-|-|-|
| &infin; | &infin; | &infin; | &infin; | &infin; | &infin; | &infin; |
| ? | ? | ? | ? | ? | ? | ? |

Designating by *i* the value returned by the previous operation, executing the sequence

```scheme
update-dist-prev-node!(i, A, 0, ?)
update-dist-prev-node!(i, C, 7, A)
update-dist-prev-node!(i, B, 9, A)
update-dist-prev-node!(i, F, 14, A)
```

changes the value of *i* to

| A | B | C | D | E | F | G |
|-|-|-|-|-|-|-|
| 0 | 9 | 7 | &infin; | &infin; | 14 | &infin; |
| ? | A | A | ? | ? | A | ? |

and returns *i*.

The operation `get-node-info(i, F)` returns the element of the type `node-info` with the following content:

| F |
| - |
| 14 |
| A |

Finally, the operation `node<dist(i, {B, C, D, E, F, G})` returns node *C*.

## 4. Steps to follow

1. It is mandatory the use of the solution of part 2. As such, after `#lang scheme` add `(require "part2.scm")`.
2. Implement and test the types of information described in section 3.
3. It is essential that you understand perfectly the Dijkstra's algorithm before developing the program. As such, you should be capable of executing the algorithm "by hand". Utilise, among others, the examples of interaction shown before.
4. Modify the presented algorithm so that, besides the shortest path between two nodes of a graph, the distances and adjacent nodes of a path are also shown, as well as the total length of the path. The form of these results should follow the indications given in section 2.
5. Implement the modified algorithm.

## 5. Incrementally determining the information associated with the nodes

If we intend to determine the shortest path between various pairs of nodes of a graph, the presented algorithm is not efficient, as it does not take advantage of the work made in determining the distance and the previous node associated with the nodes of the graph. For example, in regards to the graph in figure 2, the information associated with the nodes during the process of determining the shortest path between nodes *A* and *F* is calculated again, unnecessarily, for determining the shortest path between nodes *A* and *E*. Processing node *F* would be enough to obtain this information.

In order to avoid this situation, we can think of a procedure with internal state, that maintains a registry of the previously calculated information and uses it when it must determine the shortest path between two nodes of a graph, not calculating it again.

Hence, you should write a procedure with internal state that determines the shortest path between two nodes of a graph, just like the `shortest-path` procedure, but without calculating the information associated with the nodes more than once.

We present ahead an example of interaction with the program. Suppose that `graph1` and `graph2` correspond to the graphs of figures 1 and 2, respectively.

```scheme
> (define shortest-path1
  	(create-shortest-path-proc graph1))
> (define shortest-path2
    (create-shortest-path-proc graph2))
> (shortest-path1 'A 'C)
Processed nodes: (A B)
((A 8 B 2 C) . 10)
> (shortest-path1 'A 'B)
Processed nodes: ()
((A 8 B) . 8)
> (shortest-path2 'A 'F)
Processed nodes: (A C B)
((A 9 B 2 F) . 11)
> (shortest-path2 'A 'E)
Processed nodes: (F)
((A 9 B 2 F 9 E) . 20)
> (shortest-path2 'A 'C)
Processed nodes: ()
((A 7 C) . 7)
> (shortest-path2 'D 'F)
Processed nodes: (D E B)
((D 11 B 2 F) . 13)
```

To obtain the interaction above, you should write the procedure `(create-shortest-path-proc graph)` that receives a graph, and returns a procedure with internal state that determines the shortest path between two nodes of a `graph`, without making repeated calculations of the information associated with the nodes.

We suggest the following steps:

1. Start by identifying the information that is necessary to register, that is, the state variables that the procedure with internal state should have.
2. On a first phase, and to ease the debugging of the program, the state variables can correspond to global variables.
3. Change the Dijkstra's algorithm so that it verifies if more nodes need to be processed, or if all the necessary information was calculation in previous executions, therefore being in the state variables. The modified algorithm should register in the state variables all the new information that it calculates. **ATTENTION: You must not change the `shortest-path` procedure.**
4. Write the procedure `create-shortest-path-proc` so that the state variables that you identified become internal.

