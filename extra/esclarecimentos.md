# Esclarecimentos ao projecto
- 
Os ficheiros a entregar devem ter a extensão ".rkt" e não ".scm" como está
erradamente indicado no enunciado.

- Na __1ª fase__ do projecto não é necessário entregar um relatório.

- Ao usar o tipo lista simplificada pode-se usar a primitiva *list* como se esta pertence-se ao tipo lista simplificada.

- Implementação do tipo Lista Simplificada

Deve alterar a implementação do tipo lista simplificada apresentada no livro,
de forma a que os elementos de uma lista possam ser de qualquer tipo, e não
apenas inteiros.

Para tal, basta alterar os procedimentos "lista?" e "listas=?".

Use o predicado "equal?" para comparar elementos de listas.

- Linguagem a usar:

Deve ser escolhida a opção "Use the language declared in the source" tendo o
cuidado de escrever a seguinte linha no inicio da cada um dos seus ficheiros:
`#lang scheme`

- Exportação da definições de um módulo:

Na página 4, secção 4, do enunciado da 2ª parte do projecto no primeiro
parágrafo lê-se:
"O ﬁcheiro contendo o código deverá conter na primeira linha `#lang scheme`, na
segunda linha `(provide (all-defined-out))` e, a partir da terceira linha, em
comentário, os números e os nomes dos alunos do grupo, bem como o número do
grupo."

Deve ser colocada a linha com o "provide" de todas as definições na linha a
seguir a "#lang scheme":
```scheme
#lang scheme
(provide (all-defined-out))
```

- Tal como foi explicado nas aulas para usar ciclos é necessário carregar um
módulo adicional:

```scheme
(require (lib "misc.ss" "swindle"))
```

- Na terceira parte, ao contrário do que está dito no enunciado, não vão ser
feitos descontos por causa da utilização de caracteres acentuados nos
comentários nem nas strings.

- Na página 8 do enunciado da 3ª parte do projecto onde se lê "ATENÇÃO:
não deve alterar o procedimento caminhos-mais-curtos." deve ler-se "ATENÇÃO:
não deve alterar o procedimento caminho-mais-curto.".



