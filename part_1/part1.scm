; David Gonçalves - 2020 remake

#lang scheme

(provide (all-defined-out))

(define (prime? n)
  (define (prime-aux n i) "helper"
    (cond ((< n (* i i)) #t) ; if the divisor i has exceeded sqrt(n), then all
                             ; divisors below have been checked and n is prime
          ((zero? (remainder n i)) #f) ; if the i divides n, then n is not prime.
          (else
            (prime-aux n (+ i 1))))) ; try the next divisor
 "primality test"
 (cond ((or (not (integer? n)) (< n 1))
          (error "Number to check must be a positive integer."))
       ((< n 2) #f)
       (else
         (prime-aux n 2))))



(define (nth-prime n)
  (define (nth-prime-aux n i x p)
    (cond ((= n i) p) ; nth prime was found
          ((prime? x) (nth-prime-aux n (+ i 1) (+ x 1) x))
          (else (nth-prime-aux n i (+ x 1) p))))
 (cond ((or (not (integer? n)) (< n 1))
          (error "Number must be a positive integer."))
       (else 
         (nth-prime-aux n 0 2 2)))) ; counter i starts at 0, increases each time
                                    ; a prime number is found.
